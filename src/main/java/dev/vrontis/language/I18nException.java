package dev.vrontis.language;

/**
 * Custom exception class for cleaner overview
 */
public class I18nException extends Exception {

	/**
	 * @param message message to use for the exception
	 */
	public I18nException(String message) {
		super(message);
	}

	/**
	 * @param message message to use for the exception
	 * @param cause   throwable for the exception
	 */
	public I18nException(String message, Throwable cause) {
		super(message, cause);
	}
}
