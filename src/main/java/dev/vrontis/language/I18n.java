package dev.vrontis.language;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * "Simple" language module
 *
 * @version 0.1.0
 * @since 2020-05-06
 */
public class I18n {
	private final Logger logger = LoggerFactory.getLogger(I18n.class);
	private final BiMap<String, String> LANGUAGE_MAP = HashBiMap.create();
	private String DEFAULT_LANGUAGE;
	private String filePath = System.getProperty("user.dir") + System.getProperty("file.separator") +
			"lang" + System.getProperty("file.separator");

	/**
	 * Constructs a new i18n with the first found language as default.
	 */
	public I18n() {
		setLanguageMap();
	}

	/**
	 * Constructs a new i18n with the specified default language.
	 *
	 * @param defaultLanguage sets the default language manually
	 */
	public I18n(String defaultLanguage) {
		setLanguageMap();
		if (LANGUAGE_MAP.containsKey(defaultLanguage)) {
			DEFAULT_LANGUAGE = defaultLanguage;
		}
	}


	/**
	 * Constructs a new i18n with the specified default language and file path.
	 *
	 * @param defaultLanguage sets the default language manually
	 * @param filePath        sets the path for language files
	 */
	public I18n(String defaultLanguage, String filePath) {
		this.filePath = filePath;
		setLanguageMap();
		if (LANGUAGE_MAP.containsKey(defaultLanguage)) {
			DEFAULT_LANGUAGE = defaultLanguage;
		}
	}

	private static void main(String[] args) throws I18nException {
		I18n i18n = new I18n();

		System.out.println(i18n.getArrayByKey("test.test"));
	}

	/**
	 * Returns the default language
	 *
	 * @return String DEFAULT_LANGUAGE
	 */
	public String getDefaultLanguage() {
		return DEFAULT_LANGUAGE;
	}


	/**
	 * Returns the language string for the
	 * specified key in the default language.
	 *
	 * @param key The language key to look for
	 * @return String The string corresponding to the key
	 * @throws I18nException If the specified key does not exist
	 */
	public String getStringByKey(String key) throws I18nException {
		return getStringByKey(key, DEFAULT_LANGUAGE);
	}


	/**
	 * Returns the language string for the
	 * specified key in the specified language.
	 *
	 * @param key      The language key to look for
	 * @param language The language in which the key is looked for
	 * @return String The string corresponding to the key
	 * @throws I18nException If the specified key does not exist
	 */
	public String getStringByKey(String key, String language) throws I18nException {
		String[] path = key.split("\\.");

		String string = "";

		JsonObject current;
		try {
			current = JsonParser.parseString(LANGUAGE_MAP.get(language)).getAsJsonObject();
		} catch (Exception e) {
			throw (new I18nException("Could not parse lang file for: \"" + language + "\"", e));
		}

		for (int i = 0; i < path.length; i++) {
			if (current.has(path[i])) {
				if ((i + 1) < path.length) {
					current = current.getAsJsonObject(path[i]);
				} else {
					string = current.get(path[i]).getAsString();
				}
			} else {
				throw (new I18nException("Specified key does not exist: \"" + key + "\""));
			}
		}

		return string;
	}


	/**
	 * Returns an Array for the
	 * specified key in the default language.
	 *
	 * @param key The language key to look for
	 * @return JsonArray The array corresponding to the key
	 * @throws I18nException If the specified key does not exist
	 */
	public JsonArray getArrayByKey(String key) throws I18nException {
		return getArrayByKey(key, DEFAULT_LANGUAGE);
	}


	/**
	 * Returns an Array for the
	 * specified key in the specified language.
	 *
	 * @param key      The language key to look for
	 * @param language The language in which the key is looked for
	 * @return JsonArray The array corresponding to the key
	 * @throws I18nException If the specified key does not exist
	 */
	public JsonArray getArrayByKey(String key, String language) throws I18nException {
		String[] path = key.split("\\.");

		JsonObject current;

		JsonArray arr = null;

		try {
			current = JsonParser.parseString(LANGUAGE_MAP.get(language)).getAsJsonObject();
		} catch (Exception e) {
			throw (new I18nException("Could not parse lang file for: \"" + language + "\"", e));
		}

		for (int i = 0; i < path.length; i++) {
			if (current.has(path[i])) {
				if ((i + 1) < path.length) {
					current = current.getAsJsonObject(path[i]);
				} else {
					logger.debug("Array length: " + i);
					arr = current.getAsJsonArray(path[i]);
				}
			} else {
				throw (new I18nException("Specified key does not exist: \"" + key + "\""));
			}
		}

		if (arr == null) {
			throw (new I18nException("Specified key does not exist: \"" + key + "\""));
		}

		return arr;
	}

	private void setLanguageMap() {
		File list = new File(filePath + "languages.txt");

		InputStream isLang;

		try {
			isLang = new FileInputStream(list);
		} catch (Exception e) {
			logger.error("Does languages.txt exist?");
			e.printStackTrace();
			return;
		}

		Scanner scanner = new Scanner(isLang).useDelimiter("\\A");

		while (scanner.hasNextLine()) {
			String _s = scanner.nextLine().trim();
			if (_s.startsWith("//"))
				continue;

			String[] splitted = _s.split(" ");
			String langCode = splitted[0];
			String json;

			// Cut possible file type, all should be .lang either way
			if (langCode.endsWith(".lang"))
				langCode = langCode.split("\\.")[0];

			// Is the given lang name written according to ISO-639-1_ISO-3166-1-ALPHA-2?
			if (!Pattern.matches("[a-z][a-z]_[A-Z][A-Z]", langCode)) {
				logger.error("Language code \"" + langCode + "\" does not match the standards.");
				logger.error("Please use codes in the format of wx_YZ according to ISO-639-1 and ISO-3166-1-ALPHA-2");
				continue;
			}

			// no doubled keys
			if (LANGUAGE_MAP.containsKey(langCode)) continue;

			// Well, how about we check if the language file exists..?
			try {
				File file = new File(filePath + langCode + ".lang");

				json = JsonParser.parseReader(new FileReader(file, StandardCharsets.UTF_8)).toString();
			} catch (FileNotFoundException e) {
				logger.error("Does the language file \"" + langCode + ".lang\" exist?");
				e.printStackTrace();
				continue;
			} catch (IOException e) {
				logger.error("I've literally no Idea what this means.. " + langCode);
				e.printStackTrace();
				continue;
			}

			// if everything went well, add code and name
			LANGUAGE_MAP.put(langCode, json);

			// set a fallback default language, so that an actual language is it, till it gets set
			if (DEFAULT_LANGUAGE == null) {
				DEFAULT_LANGUAGE = langCode;
			}
		} // end while

		scanner.close();
	}
}
